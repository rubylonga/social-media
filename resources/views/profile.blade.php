@extends('layout.master');

@section('content')
    <div class="container">
            <h2>Pengisian data Profile</h2><br>
            @if (empty($ambil_profile))
                <form action="/profile" method="POST">
                    @csrf
                    <input type="hidden" value="{{Auth::user()->id}}" name="user_id">
                    <div class="form-group">
                        <label for="umur">Umur</label>
                        <input type="text" class="form-control" name="umur" id="umur" placeholder="Masukkan Umur">
                        @error('umur')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="alamat">Alamat</label>
                        <input type="text" class="form-control" name="alamat" id="alamat" placeholder="Masukkan Alamat">
                        @error('alamat')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                  
                         <button type="submit" class="btn btn-primary">Tambah</button>
                        </form>
                        
                    @else
                            <label for="umur">Umur</label>
                            <input type="text" name="umur" id="umur" readonly value="{{$ambil_profile->umur}}" class="form-control"><br>
                            <label for="alamat">Alamat</label>
                            <input type="text" name="alamat" id="alamat" readonly value="{{$ambil_profile->alamat}}" class="form-control">
                            <a href="{{url('/profile/edit/'.$ambil_profile->id)}}" class="btn btn-primary mt-3">Edit</a>
                    @endif
                   
             


    </div>
@endsection