@extends('layout.master');

@section('content')
<div class="container">
    <a href="{{url('/kategori/create')}}" class="btn btn-info mt-2">Tambah Data Kategori</a>
    <table class="table table-striped mt-2">
        <thead>
            <tr>
                <th>Nama Kategori</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($kategori as $item)
            <tr>
                <td>{{$item->nama}}</td>
                <td>
                    <form action="/kategori/{{$item->id}}" method="POST">
                        <a href="/kategori/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
      </table>
</div>
@endsection