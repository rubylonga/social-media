@extends('layout.master');

@section('content')
    <div class="container">
            <h2>Tambah Data Kategori</h2>
                {{-- <form action="/berita" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan Judul">
                        @error('judul')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="content">Content</label><br>
                        <textarea name="konten" id="content" cols="30" rows="10"></textarea>
                        @error('konten')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="penulis">Penulis</label><br>
                        <input type="text" class="form-control" name="penulis" id="penulis" placeholder="Masukkan penulis">
                        @error('bio')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </form> --}}

                <form action="/kategori" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="kategori">Nama Kategori</label>
                        <input type="text" class="form-control" name="nama" id="kategori" placeholder="Masukkan kategori">
                        @error('nama')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </form>
    
    </div>
@endsection