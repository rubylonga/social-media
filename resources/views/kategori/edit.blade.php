@extends('layout.master');

@section('content')
    <div class="container">
        <div>
            <form action="/kategori/{{$kt->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="nama">Nama Kategori</label>
                    <input type="text" class="form-control" name="nama" value="{{$kt->nama}}" id="nama" placeholder="Masukkan Kategori">
                    @error('nama')
                        <div class="alert alert-danger">
                            {{ $message }}
                        </div>
                    @enderror
                </div>
                <button type="submit" class="btn btn-primary">Edit</button>
            </form>
        </div>
    </div>
@endsection