@extends('layout.master')

@section('content')
    <div class="container">
        <form action="/profile/{{$profiles->id}}" method="POST">
            @csrf
            @method('PUT')
            <label for="umur">Umur</label>
            <input type="text" name="umur" id="umur" value="{{$profiles->umur}}" class="form-control"><br>
            <label for="alamat">Alamat</label>
            <input type="text" name="alamat" id="alamat"  value="{{$profiles->alamat}}" class="form-control">
            <button type="submit" class="btn btn-primary mt-2">Edit</button>
        </form>
    </div>
@endsection