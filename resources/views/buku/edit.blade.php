@extends('layout.master');

@section('content')
    <div class="container">
            <h2>Edit buku<h2>
                {{-- <form action="/berita" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan Judul">
                        @error('judul')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="content">Content</label><br>
                        <textarea name="konten" id="content" cols="30" rows="10"></textarea>
                        @error('konten')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="penulis">Penulis</label><br>
                        <input type="text" class="form-control" name="penulis" id="penulis" placeholder="Masukkan penulis">
                        @error('bio')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </form> --}}

                <form action="/buku/{{$bk->id}}" method="POST" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')
                    <div class="form-group">
                        <label for="kategori">Pilih Kategori</label>
                        <select name="kategori" class="form-control" id="kategori">
                            <option value="#">-- Pilih Kategori --</option>
                            @foreach ($kt as $as)
                            <option value="{{$as->id}}" {{($bk->id_kategori == $as->id) ? 'selected' : ''}}>{{$as->nama}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="nama_buku">Nama Buku</label>
                        <input type="text" class="form-control" name="nama_buku" id="nama_buku" placeholder="Masukkan Nama Buku" value="{{$bk->nama_buku}}">
                        @error('nama_buku')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="penulis">Penulis</label>
                        <input type="text" class="form-control" name="penulis" id="penulis" placeholder="Penulis" value="{{$bk->penulis}}">
                        @error('penulis')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="gambar">Gambar</label><br>
                        <img src="{{asset('img/'. $bk->gambar)}}" alt="" width="250px">
                        <input type="file" class="form-control mt-2" name="gambar" id="gambar" placeholder="Masukkan Gambar" >
                        @error('gambar')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <button type="submit" class="btn btn-primary">Tambah</button>
                </form>
    
    </div>
@endsection