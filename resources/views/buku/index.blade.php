@extends('layout.master');

@section('content')
<div class="container">
    <a href="{{url('/buku/create')}}" class="btn btn-info mt-2">Tambah Data Buku</a>
    <table class="table table-striped mt-2">
        <thead>
            <tr>
                <th>Nama Kategori</th>
                <th>nama Buku</th>
                <th>penulis</th>
                <th>Gambar</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($ambil_buku as $item)
            <tr>
                <td>{{$item->Kategori->nama}}</td>
                <td>{{$item->nama_buku}}</td>
                <td>{{$item->penulis}}</td>
                <td><a href="{{url("img/$item->gambar")}}">Gambar</a></td>
                <td>
                    <form action="/buku/{{$item->id}}" method="POST">
                        <a href="/buku/{{$item->id}}/edit" class="btn btn-primary">Edit</a>
                        @csrf
                        @method('DELETE')
                        <input type="submit" class="btn btn-danger my-1" value="Delete">
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
      </table>
</div>
@endsection