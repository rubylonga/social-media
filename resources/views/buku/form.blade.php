@extends('layout.master');

@section('content')
    <div class="container">
            <h2>Tambah Data Buku</h2>
                {{-- <form action="/berita" method="POST">
                    @csrf
                    <div class="form-group">
                        <label for="judul">Judul</label>
                        <input type="text" class="form-control" name="judul" id="judul" placeholder="Masukkan Judul">
                        @error('judul')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="content">Content</label><br>
                        <textarea name="konten" id="content" cols="30" rows="10"></textarea>
                        @error('konten')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <div class="form-group">
                        <label for="penulis">Penulis</label><br>
                        <input type="text" class="form-control" name="penulis" id="penulis" placeholder="Masukkan penulis">
                        @error('bio')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </form> --}}

                <form action="/buku" method="POST" enctype="multipart/form-data">
                    @csrf
                    <div class="form-group">
                        <label for="kategori">Pilih Kategori</label>
                        <select name="kategori" class="form-control" id="kategori">
                            <option value="#">-- Pilih Kategori --</option>
                            @foreach ($kt as $as)
                            <option value="{{$as->id}}">{{$as->nama}}</option>
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group">
                        <label for="nama_buku">Nama Buku</label>
                        <input type="text" class="form-control" name="nama_buku" id="nama_buku" placeholder="Masukkan Nama Buku">
                        @error('nama_buku')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="penulis">Penulis</label>
                        <input type="text" class="form-control" name="penulis" id="penulis" placeholder="Penulis">
                        @error('penulis')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>

                    <div class="form-group">
                        <label for="gambar">Gambar</label>
                        <input type="file" class="form-control" name="gambar" id="gambar" placeholder="Masukkan Gambar">
                        @error('gambar')
                            <div class="alert alert-danger">
                                {{ $message }}
                            </div>
                        @enderror
                    </div>
                    <button type="submit" class="btn btn-primary">Tambah</button>
                </form>
    
    </div>
@endsection