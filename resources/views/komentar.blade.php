@extends('layout.master')

@section('content')
@if (empty($pj->komentar))
    <div class="container">
        <img height="200" src="{{asset("img/". $pj->Buku->gambar)}}" alt="">
        <form action="/komentar/{{$pj->id}}" method="POST">
        @csrf
        @method('PUT')
            <label for="komen">Masukkan Komentar mu disini</label><br>
            <textarea name="komentar" id="komen" cols="30" rows="10"></textarea><br>
            <button type="submit" class="btn btn-primary">Simpan</button>
        </form>
    </div>
@else
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="d-flex flex-column align-items-center">
                    <h4>Sudah ada komentar untuk buku ini </h4>
                    <a class="btn btn-dark" href="{{url('/data_buku')}}">Kembali</a>
                </div>
            </div>
        </div>
    </div>
@endif
    
@endsection