<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="#" class="brand-link">
      <img src="{{asset('/adminlte/dist/img/AdminLTELogo.png')}}" alt="AdminLTE Logo" class="brand-image img-circle elevation-3" style="opacity: .8">
      <span class="brand-text font-weight-light">Peminjaman` Online</span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
      <!-- Sidebar user (optional) -->
      <div class="user-panel mt-3 pb-3 mb-3 d-flex">
        <div class="image">
          <img src="{{asset('/adminlte/dist/img/user2-160x160.jpg')}}" class="img-circle elevation-2" alt="User Image">
        </div>
        <div class="info">
          <a href="#" class="d-block"> {{ Auth::user()->name }}</a>
        </div>
      </div>


      <!-- Sidebar Menu -->
      <nav class="mt-2">
        <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
          <!-- Add icons to the links using the .nav-icon class
               with font-awesome or any other icon font library -->
          <li class="nav-item">
            <a href="/" class="nav-link">
              <i class="nav-icon fas fa-tachometer-alt"></i>
              <p>
                Dashboard
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/kategori" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                  List Kategori Untuk Admin (HANYA BISA DIAKSES ADMIN)
              </p>
            </a>
          </li>
          <li class="nav-item">
            <a href="/buku" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                List Buku untuk Admin (HANYA BISA DIAKSES ADMIN)
              </p>
            </a>
          </li>  

          <li class="nav-item">
            <a href="/data_peminjaman" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                List Data buku yang dipinjam oleh user,hanya  untuk Admin (HANYA BISA DIAKSES ADMIN)
              </p>
            </a>
          </li>  

          <li class="nav-item">
            <a href="/profile" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Profile
              </p>
            </a>
          </li>  


          <li class="nav-item">
            <a href="/data_buku" class="nav-link">
              <i class="nav-icon fas fa-copy"></i>
              <p>
                Data buku yang dipinjam
              </p>
            </a>
          </li>  

          <li class="nav-item">
            <a class="dropdown-item" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                        document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
            </a>
            
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                 @csrf
            </form>
          </li> 
        </ul>
      </nav>
      <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
  </aside>