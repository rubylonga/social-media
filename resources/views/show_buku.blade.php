@extends('layout.master')

@section('content') 
    @if ($bk->status == 1)
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="d-flex flex-column align-items-center">
                    <h4>Sudah dipinjam nih </h4>
                    <a class="btn btn-dark" href="{{url('/')}}">Kembali</a>
                </div>
            </div>
        </div>
      </div>
     @else
    <div class="container">
        <form action="/peminjaman/{{$bk->id}}" method="POST">
            @csrf
            <div class="card">
                <div class="card-header">
                {{$bk->nama_buku}}
                </div>
                <div class="card-body">
                    <input type="hidden" name="buku_id" id="buku_id" value="{{$bk->id}}">
                    <img src="{{asset('img/'. $bk->gambar)}}" alt="" width="30%">
                <blockquote class="blockquote mb-0">
                    penulis :  {{$bk->penulis}}
                </blockquote>
                <button type="submit" class="btn btn-primary">Anda yakin ingin pinjam</button>
                </div>
            </div>
        </form>
    </div>
    @endif
@endsection