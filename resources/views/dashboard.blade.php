@extends('layout.master');

@section('content')
   <div class="container">
       <div class="row">
        @foreach ($buku as $item)
           <div class="col-md-4">
                <div class="card" style="width: 18rem;">
                    <img class="card-img-top" src="{{asset('img/'. $item->gambar)}}" alt="Card image cap">
                    <div class="card-body">
                        <p class="btn btn-info">Kategori : {{$item->Kategori->nama}}</p><br>
                        <h5 class="card-title">{{$item->nama_buku}}</h5><br>
                        <h5> Penulis : {{$item->penulis}}</h5>
                        {{-- <p>Stok : {{$item->stok}}</p> --}}
                        @if (empty($ambil_user->umur) && empty($ambil_user->alamat))
                            <a href="{{url('/profile')}}" class="btn btn-primary">Jika Ingin Pinjam Harap Masukkan Profile Terlebih dahulu</a>
                        @else
                            <a href="/buku/{{$item->id}}" class="btn btn-info">pinjam</a>
                        @endif
                      
                    </div>
                </div>
           </div>
        @endforeach
       </div>
   </div>
@endsection