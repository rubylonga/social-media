@extends('layout.master')

@section('content')
    <div class="container">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>Gambar</th>
                    <th>Nama Buku</th>
                    <th>Penulis</th>
                    <th>Tambahkan Komentar untuk buku ini</th>
                    <th>Aksi</th>
                </tr>
            </thead>
            @foreach ($peminjaman as $item)
            <tbody>
                <tr>
                    <td><img src="{{asset('img/'. $item->Buku->gambar)}}" width="150px" alt=""></td>
                    <td>{{$item->Buku->nama_buku}}</td>
                    <td>{{$item->Buku->penulis}}</td>
                    <td>
                        <a href="{{url('komentar/'. $item->id)}}">komentar</a>
                    </td>
                    <td>
                        <a href="{{url('kembalikan/'.  $item->id)}}">Kembalikan</a>
                    </td>
                </tr>
            </tbody>
            @endforeach
        </table>
    </div>
    @push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.10.24/datatables.min.css"/> 
    @endpush

    @push('script')
      <script src="{{asset('/adminlte/plugins/datatables/jquery.dataTables.js')}}"></script>
      <script src="{{asset('/adminlte/plugins/datatables-bs4/js/dataTables.bootstrap4.js')}}"></script>
      <script>
        $(function () {
          $("#example1").DataTable();
        });
      </script>
    @endpush
@endsection