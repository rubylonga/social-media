<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Buku;
use App\User;

class Peminjaman extends Model
{
    protected $fillable = [
        'id_buku',
        'user_id',
        'komentar'
    ];

    protected $table = 'peminjamans';


    public function Buku(){
        return $this->belongsTo(Buku::class,'id_buku');
    }

    public function User(){
        return $this->belongsTo(User::class,'user_id');
    }
}
