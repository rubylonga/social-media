<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\User;

class Profile extends Model
{

    protected $fillable = [
        'user_id',
        'umur',
        'alamat'
    ];

    public function User(){
        return $this->belongsTo(User::class,'user_id');
    }
}
