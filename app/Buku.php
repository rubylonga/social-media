<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Kategori;

class Buku extends Model
{
    protected $fillable = [
        'id_kategori',
        'nama_buku',
        'penulis',
        'gambar',
        'status'
    ];

    public function Kategori(){
        return $this->belongsTo(Kategori::class,'id_kategori');
    }

    public function Peminjaman(){
        return $this->hasMany(Peminjaman::class,'id_buku');
    }
}
