<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kategori;

class KategoriController extends Controller
{
    public function index(){
        $kategori = Kategori::all();
        return view('kategori.index',compact('kategori'));
    }

    public function create(){
        return view('kategori.form');
    }

    public function store(Request $request){
        $request->validate([
            'nama' => 'required|unique:kategoris',
        ]);
       

        $query = Kategori::create([
            'nama' => $request->nama,
        ]);
        return redirect('/kategori')->with('success','berhasil membuat kategori baru');
    }

    public function edit($id){
        $kt = Kategori::find($id);
        return view('kategori.edit', compact('kt'));
    }

    public function update($id,Request $request){
        $ktgori = Kategori::find($id);
        $ktgori->nama = $request->nama;
        $ktgori->save();
        return redirect('/kategori')->with('success','berhasil mengganti kategori');
    }

    public function delete($id){
        Kategori::find($id)->delete();
        return redirect('/kategori')->with('success','berhasil menghapus kategori');
    }
}
