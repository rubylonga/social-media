<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Profile;
use Auth;

class ProfileController extends Controller
{
    public function index(){
        $ambil_profile = Profile::where('user_id', Auth::user()->id)->first();
        return view('profile',compact('ambil_profile'));
    }

    public function store(Request $request){
        $request->validate([
            'umur' => 'required',
            'alamat' => 'required'
        ]);
        
        $query = Profile::create([
            'user_id' => $request->user_id,
            'umur' => $request->umur,
            'alamat' => $request->alamat
        ]);

        return redirect('/profile')->with('success','data berhasil diisi');
    }

    public function edit($id){
        $profiles = Profile::find($id);
        return view('edit_profile',compact('profiles'));
    }

    public function update(Request $request, $id){
        $profile = Profile::find($id);
        $profile->umur = $request->umur;
        $profile->alamat = $request->alamat;
        $profile->save();
        return redirect('/profile')->with('success','profile berhasil diganti');
    }
}
