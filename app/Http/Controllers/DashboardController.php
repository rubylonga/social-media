<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buku;
use App\User;
use App\Profile;
use Auth;
use App\Peminjaman;

class DashboardController extends Controller
{
    public function index(){
        $buku =Buku::all();
        $ambil_user = Profile::where('user_id', Auth::user()->id)->first();
        return view('dashboard',compact('buku','ambil_user'));
    }

    public function show($id){
        $bk = Buku::find($id);
        return view('show_buku',compact('bk'));
    }

    public function ambil(){
        $user = User::where('id',Auth::user()->id)->first();
        $peminjaman = Peminjaman::where('user_id',$user->id)->get();
        return view('data-buku',compact('user','peminjaman'));
    }

    public function komentar($id){
        $pj = Peminjaman::find($id);
        return view('komentar',compact('pj'));
    }

    public function update(Request $request,$id){
        $peminjamans = Peminjaman::find($id);
        $peminjamans->komentar = $request->komentar;
        $peminjamans->save();
        return redirect('/')->with('success','sukses memasukkan komentar');
    }

    public function kembali($id){
        $pinjam = Peminjaman::find($id);
        $bk  = Buku::where('id',$pinjam->id_buku)->first();
        $bk->status = 0;
        $bk->update();
        $pinjam->delete();

        return redirect('/')->with('success','Buku yang anda pinjam berhasil dibalikkin');
    }
}
