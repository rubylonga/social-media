<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Buku;
use App\Kategori;
use File;

class BukuController extends Controller
{
    public function index(){
        $ambil_buku = Buku::all();
        return view('buku.index',compact('ambil_buku'));
    }

    public function create(){
        $kt = Kategori::all();
        return view('buku.form',compact('kt'));
    }

    public function store(Request $request){
        $file = $request->file('gambar');
        $nama_file = $file->getClientOriginalName();
        $tujuan_upload = 'img';
        $file->move($tujuan_upload,$nama_file);
    
        Buku::create([
            'id_kategori'   => $request->kategori,
            'nama_buku'   => $request->nama_buku,
            'penulis'        => $request->penulis,
            'gambar'           => $nama_file,
            'status' => 0
        ]);

        return redirect('/buku')->with('success','data buku berhasil dimasukkan');
    }

    public function edit($id){
        $bk = Buku::find($id);
        $kt = Kategori::all();
        return view('buku.edit',compact('bk','kt'));
    }

    public function update(Request $request, $id){
        $buku = Buku::findorfail($id);
            if($request->has('gambar')){
                $awal = $buku->gambar;
                $buku_data = [
                    'id_kategori'   => $request->kategori,
                    'nama_buku'   => $request->nama_buku,
                    'penulis'        => $request->penulis,
                    'gambar'           => $awal,
                ];
                $request->gambar->move(public_path().'/img', $awal);
               
            }else{
                $buku_data = [
                    'id_kategori'   => $request->kategori,
                    'nama_buku'   => $request->nama_buku,
                    'penulis'        => $request->penulis,
                ];
            }
       
            $buku->update($buku_data);
       
            return redirect('/buku')->with('success','data berhasil diupdate');


    //    if($request->has('gambar')){
    //        $path = 'img';
    //        File::delete($path . $buku->gambar);
    //        $gambar = $request->gambar;
    //        $new_gambar = time() . ' - ' . $gambar->getClientOriginalName();
    //        $gambar->move($path,$new_gambar);
    //        $buku_data = [
    //            'id_kategori' => $request->kategori,
    //            'nama_buku'  => $request->nama_buku,
    //            'penulis' => $request->penulis,
    //            'gambar' => $new_gambar
    //        ];
    //    }else{
    //         $buku_data = [
    //             'id_kategori' => $request->kategori,
    //             'nama_buku'  => $request->nama_buku,
    //             'penulis' => $request->penulis,
    //         ];
    //    }
    //    $buku->save($buku_data);  
    }
    
    public function delete($id){
        Buku::find($id)->delete();
        return redirect('/buku')->with('success','data berhasil dihapus');
    }
}
