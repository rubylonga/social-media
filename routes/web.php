<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


//     Route::get('/', function () {
//     return view('welcome');
// });
 

Route::group(['middleware' => ['auth','ceklevel:admin']], function () {
    
    //untuk kategori
    Route::get('/kategori','KategoriController@index');
    Route::get('/kategori/create','KategoriController@create');
    Route::post('/kategori','KategoriController@store');
    Route::get('/kategori/{id}/edit','KategoriController@edit');
    Route::put('/kategori/{id}','KategoriController@update');
    Route::delete('/kategori/{id}', 'KategoriController@delete');

    //untuk tambah buku
    Route::get('/buku','BukuController@index');
    Route::get('/buku/create','BukuController@create');
    Route::post('/buku','BukuController@store');
    Route::get('/buku/{id}/edit','BukuController@edit');
    Route::put('/buku/{id}','BukuController@update');
    Route::delete('/buku/{id}','BukuController@delete');

    //ambil data peminjaman
    Route::get('/data_peminjaman','PeminjamanController@show');
});

Route::group(['middleware' => ['auth','ceklevel:admin,user']], function () {
    Route::get('/','DashboardController@index');
    Route::get('/pinjam_buku','DashboardController@pinjam');

    //untuk isi profile
    Route::get('/profile','ProfileController@index');
    Route::post('/profile','ProfileController@store');
    Route::get('/profile/edit/{id}','ProfileController@edit');
    Route::put('/profile/{id}','ProfileController@update');

    //untuk masuk detail buku untuk pinjam
    Route::get('/buku/{id}', 'DashboardController@show');

    //untuk peminjaman
    Route::post('/peminjaman/{id}','PeminjamanController@store');

    //untuk liat data buku yang dipinjam
    Route::get('/data_buku','DashboardController@ambil');

    //untuk tambah komentar
    Route::get('/komentar/{id}','DashboardController@komentar');
    Route::put('/komentar/{id}', 'DashboardController@update');

    //untuk pengembalian buku
    Route::get('/kembalikan/{id}', 'DashboardController@kembali');

});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
